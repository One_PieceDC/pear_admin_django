// - 用于表单项的提示信息
let $ = layui.$;
$("*[lay-tips]")
  .on("mouseenter", function () {
    let content = $(this).attr("lay-tips");
    this.index = layer.tips('<div style="padding: 0px; font-size: 14px; color: #eee;">' + content + "</div>", this, {
      time: -1, maxWidth: 800, tips: [1, "#3A3D49"],
    },);
  })
  .on("mouseleave", function () {
    layer.close(this.index);
  });


// - 解决csrf跨域问题
function solve_csrf(token) {
  $.ajaxSetup({
    headers: {'X-CSRFTOKEN': `${token}`}, // 这里是headers,不是data -- CSRF
  });
}


// - 新增、编辑、授权等iframe窗口的大小设置
function layer_open(title, url, width = '450px', height = '350px') {
  return layer.open({
    title: title,
    type: 2,
    maxmin: true, // 是否开启标题栏的最大化和最小化图标。
    shadeClose: true, // 点击遮罩区域，关闭弹层
    shade: 0.3, // 遮罩透明度
    area: [width, height], // iframe框的宽高
    offset: "15%", // iframe距离上面的距离
    content: url,
  });
}

function ajaxError(popup, XMLHttpRequest, textStatus, errorThrown) {
  let status_code = XMLHttpRequest.status
  // get相关的url - 后端401直接重定向登录页面了;404后端直接呈现找不到的提示页面
  // post相关的url -前端拿到403的响应状态码,弹出无权访问的提示信息;前端拿到500的响应状态码,弹出服务器异常的提示信息;
  // 其他的状态码,弹出系统异常的提示信息
  if (status_code === 403) {
    popup.failure("您无权进行该操作,请联系管理员!");
  } else if (status_code === 500) {
    popup.failure("服务器异常,请联系管理员!");
  } else {
    popup.failure("系统异常,请联系管理员!");
  }
}


function loginAjaxRequest(btn, popup, url, d) {
  $.ajax({
    url: url,
    type: "post",
    data: d,
    contentType: "application/x-www-form-urlencoded",
    success: function (data) {
      btn.stop();
      if (data.code === 0) {
        layer.msg(data.msg, {icon: 1, time: 1500}, function () {
          // 将当前登陆用户的信息保存到本地
          localStorage.setItem("current_user_info", data.data.user_info);
          location.href = "/";
        })
      } else {
        popup.failure(data.msg);
      }
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      let status_code = XMLHttpRequest.status
      popup.failure("系统异常,请联系管理员!");
    },
  });
}

// - 删除的ajax请求!
function deleteAjaxRequest(popup, obj, url, data) {
  let loading = layer.load()
  $.ajax({
    url: url,
    type: "post",
    contentType: "application/x-www-form-urlencoded",
    data: data,
    success: function (data) {
      layer.close(loading);
      if (data.code === 0) {
        popup.success(data.msg, function () {
          obj.del()
        })
      } else {
        popup.failure(data.msg)
      }
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      layer.close(loading);
      ajaxError(popup, XMLHttpRequest, textStatus, errorThrown);
    },
  })
}

// - 新增和编辑的ajax请求! > 其实只要是form表单提交,都可以用封装好的这个函数!
function addAndEditAjaxRequest(popup, url, d, reload_table) {
  $.ajax({
    url: url,
    type: "post",
    data: d,
    contentType: "application/x-www-form-urlencoded",
    success: function (data) {
      if (data.code === 0) {
        layer.msg(data.msg, {icon: 1, time: 1500}, function () {
          parent.layer.close(parent.layer.getFrameIndex(window.name)); //关闭当前页
          parent.layui.table.reload(reload_table); // 刷新表格
        });
      } else {
        popup.failure(data.msg);
      }
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      ajaxError(popup, XMLHttpRequest, textStatus, errorThrown);
    },
  });
}

// - 复选框事件的ajax请求 (就表格上的那个 启用/禁用 按钮)
function switchAjaxRequest(popup, url, data, reload_table) {
  let loading = layer.load()
  $.ajax({
    url: url,
    type: "post",
    contentType: "application/x-www-form-urlencoded",
    data: data,
    success: function (data) {
      layer.close(loading);
      if (data.code === 0) {
        popup.success(data.msg);
      } else {
        layer.msg(data.msg, {
          icon: 2,
          time: 2500
        })
      }
      layui.table.reloadData(reload_table); // 无感刷新权限树表格的数据！
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      layer.close(loading);
      let status_code = XMLHttpRequest.status
      if (status_code === 403) {
        popup.failure("您无权进行该操作,请联系管理员!");
        layui.table.reloadData(reload_table); // 无感刷新权限树表格的数据！
      } else if (status_code === 500) {
        popup.failure("服务器异常,请联系管理员!");
      } else {
        popup.failure("系统异常,请联系管理员!");
      }
    },
  })
}

// 第三方组件(combotree)的下拉树
function combotree_init(combotree, elem, url, initValue = "") {
  combotree.render({
    elem: elem
    , isMultiple: false
    , expandLevel: 2
    , yChkboxType: 's'
    , nChkboxType: 's'
    , ajaxUrl: url
    , initValue: initValue  // 默认选中的节点!
  })
}

// pear里的组件(iconPicker)图标选择器
function iconPicker_init(iconPicker, elem) {
  iconPicker.render({
    elem: elem,  // 选择器,推荐使用input
    type: 'fontClass',  // 数据类型: fontClass/unicode,推荐使用fontClass
    search: true,  // 是否开启搜索: true/false,默认true
    page: true,  // 是否开启分页: true/false,默认true
    limit: 12,  // 每页显示数量,默认12
  });
}


// 递归获取layui的tree组件中选中的那些节点的id
function get_layui_tree_id(list_tree_data) {
  if (Array.isArray(list_tree_data)) {
    let ret = [];
    $.each(list_tree_data, function (index, item) {
      if (item?.children) {
        ret.push(...get_layui_tree_id(item.children));
      }
      ret.push(...get_layui_tree_id(item));
    });
    return ret;
  } else {
    return [list_tree_data.id];
  }
}