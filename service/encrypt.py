import hashlib
from django.conf import settings


def md5(data_string):
    obj = hashlib.md5(settings.SECRET_KEY.encode('utf-8'))
    obj.update(data_string.encode('utf-8'))
    return obj.hexdigest()


""" 创建了测试数据:
models.Company.objects.create(
        name="大和实业", mobile="13888888881", 
        password=md5("root123"), auth_type=2, ctime=datetime.now())
"""
