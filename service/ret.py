"""code返回码"""
SUCCESS = 0  # 成功
FIELD_ERROR = -1  # modelform表单校验失败,ps:在字段钩子里可能有查询数据库
SUMMARY_ERROR = -2  # 在视图函数里写了查询数据库的逻辑
NOT_FOUND = -3  # 对象不存在
ILLEGAL_ERROR = -4  # 非法操作

UPLOAD_OVERSIZE = -5  # 上传的图片太大
UPLOAD_ERROR = -100  # 上传图片失败

Baidu_AI = -101  # 百度AI识别失败
