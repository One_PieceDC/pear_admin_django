from django.conf import settings

from apps.base.models import Rights


def init_permission(current_user, request):
    """拿到当前登陆用户的所有权限,并存入Django的session表中!"""
    rights_obj_list = set()  # 一个用户可拥有多个角色,角色拥有的权限可能重复,所以用集合来去重
    for role_obj in current_user.role_list.filter(is_deleted=False, is_enabled=True, rights_list__isnull=False):
        for rights_obj in role_obj.rights_list.filter(is_deleted=False, is_enabled=True):
            if rights_obj.kind != "menu":
                rights_obj_list.add(rights_obj)

    permissions_list = []
    for item in rights_obj_list:
        permissions_list.append(item.url)
    request.session[settings.PERMISSION_SESSION_KEY] = permissions_list
