from django.http import JsonResponse
from service import ret


# JsonResponse的status默认200,ajax的success分支接收,显式指定status不等于200,则ajax的error分支接收返回的信息
# > !! 返回列表信息的func的参数是 code、msg、data;
#      返回form表单校验信息的func的参数是 form;
#      其余的func的参数都是 code、msg;

def success_api(msg="操作成功!"):
    """用于新增、修改、删除成功时,返回信息"""
    return JsonResponse({"code": ret.SUCCESS, "msg": msg})  # 0


def table_api(msg="ok", data=None, count=0):
    """用于返回 表格/列表 数据信息!!"""
    if data is None:
        data = []
    return JsonResponse({"code": ret.SUCCESS, "msg": msg, "data": data, "count": count})  # 0


def tree_api(msg="ok", data=None, select=None):
    """用于返回 树,并默认勾选一些节点 !!"""
    if data is None:
        data = []
    if select is None:
        select = []
    return JsonResponse({"code": ret.SUCCESS, "msg": msg, "data": data, "select": select})  # 0


def no_valid_api(form):
    """用于modelform组件校验不通过,返回错误信息"""
    # clean整体校验的错误
    if "__all__" in form.errors.get_json_data():
        msg = form.errors.get_json_data()["__all__"][0]["message"]
    else:
        # 字段自身校验的错误
        error_data = form.errors.get_json_data()
        key, message = list(error_data.items())[0]
        msg = f"{key}{message[0]['message']}"
    return JsonResponse({'code': ret.FIELD_ERROR, 'msg': msg})  # -1


def summary_error_api(msg="匹配失败"):
    """多用于查询数据库,查询多个字段没有结果 编码位置“视图函数中” - SUMMARY_ERROR - -2"""
    return JsonResponse({"code": ret.SUMMARY_ERROR, "msg": msg})  # 用户名或密码错误 -2


def not_found_api(msg='不存在!'):
    """多用于查询数据库,未找到/对象不存在 编码位置“视图函数中” - NOT_FOUND - -3"""
    return JsonResponse({"code": ret.NOT_FOUND, "msg": msg})  # xxx不存在  -3


def illegal_api(msg='非法操作!'):
    """当前登陆用户只能在个人中心中修改自己的信息 编码位置“视图函数中” - NOT_FOUND - -3"""
    return JsonResponse({"code": ret.ILLEGAL_ERROR, "msg": msg})  # 非法操作  -4
