"""后面我思考了下,用jwt没有session来处理好使,遂放弃这个文件的使用 改用session"""
import jwt
import datetime
from jwt import exceptions
from django.conf import settings


def create_token(payload, timeout=15):
    """
    :param payload: 以字典格式存储的用户信息,比如{'user_id':1,'username':'dc'} 必传!
    :param timeout: token的过期时间,默认设置为20分钟
    :return:
    """
    headers = {
        'typ': 'jwt',
        'alg': 'HS256'
    }
    payload['exp'] = datetime.datetime.utcnow() + datetime.timedelta(hours=timeout)  # seconds 秒; hours 小时
    # 盐使用的是Django配置文件中的密钥SECRET_KEY
    result = jwt.encode(headers=headers, payload=payload, key=settings.SECRET_KEY.encode('utf-8'), algorithm="HS256")
    return result


def parse_payload(token):
    """ 对jwt token校验,校验成功返回元祖 (True/False , payload值/验证失败信息) """
    try:
        verified_payload = jwt.decode(token, settings.SECRET_KEY.encode('utf-8'), algorithms=["HS256"])
        return True, verified_payload
    except exceptions.ExpiredSignatureError:
        error = 'token已失效'
    except jwt.DecodeError:
        error = 'token认证失败'
    except jwt.InvalidTokenError:
        error = '非法的token'
    return False, error


"""
- 登陆时
# ！返回前端jwt_token + 当前用户所拥有的权限放到Django的session表中
token = create_token({'user_id': u_obj.id, 'name': u_obj.username})
# init_permission(u_obj, request)
return JsonResponse({
    "code": ret.SUCCESS,
    'msg': "登陆成功!",
    'data': {"token": token, 'user_info': u_obj.json()}
})

- 在django中间件中可通过jwt来进行登陆验证
print(request.META)
authorization = request.META.get('HTTP_AUTHORIZATION', '')
if not authorization:
    return JsonResponse({'code': ret.NO_TOKEN, 'msg': '请登录后访问'})
status, info_or_error = parse_payload(authorization)
if not status:
    return JsonResponse({'code': ret.TOKEN_ERROR, 'msg': info_or_error + "，请重新登录"})
# 校验成功 给request.user赋值为当前登陆用户的id
request.user = info_or_error['user_id']

NO_TOKEN = -3  # 未登陆不能访问/没有携带token信息
TOKEN_ERROR = -4  # token过期/token校验失败/非法token

- 退出登陆时
def logout(request):
    # To do(可有可无):jwt的撤销与黑名单. 后端来控制,撤销后就不允许它用之前的token了,用redis来实现.
    return JsonResponse({
        "code": ret.SUCCESS,
        'msg': "注销成功!",
    })
"""
