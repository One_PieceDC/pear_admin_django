DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pear_admin_django',  # -- 库名
        'USER': 'root',  # -- 公司里不会用root用户
        'PASSWORD': 'admin1234',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'CHARSET': 'utf8'
    }
}
