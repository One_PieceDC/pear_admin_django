# from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.views.static import serve


def test(request):
    from django.http import JsonResponse
    return JsonResponse({"msg": "Hello World!"})


urlpatterns = [
    path('', include('apps.rbac.urls')),
    re_path(r"^media/(?P<path>.*)$", serve, {'document_root': settings.MEDIA_ROOT}, name="media"),
    path('test/', test),
]
