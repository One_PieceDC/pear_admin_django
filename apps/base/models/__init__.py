from .department import Department
from .rights import Rights
from .role import Role
from .user import User
from .log import Log

from django.db import models


class Test(models.Model):
    user = models.CharField(verbose_name="操作者", max_length=255)
    msg = models.TextField(verbose_name="信息")
    sort = models.IntegerField(verbose_name="顺序")


