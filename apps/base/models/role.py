from django.db import models

from ._base import BaseORM
from .rights import Rights


class Role(BaseORM):
    name = models.CharField(verbose_name="角色名称", max_length=20)
    code = models.CharField(verbose_name="角色标识符", max_length=20)
    desc = models.TextField(verbose_name="角色描述", blank=True, null=True)
    is_enabled = models.BooleanField(verbose_name="角色是否可用 0-禁用 1-可用", default=True)

    rights_ids = models.CharField(
        verbose_name="权限ids,1,2,5",
        max_length=512,
        blank=True,
        null=True,
        help_text="冗余字段,用户缓存用户权限",
    )

    rights_list = models.ManyToManyField(verbose_name='拥有的所有权限', to=Rights, blank=True, db_table='ums_role_rights')

    class Meta:
        db_table = 'ums_role'

    def json(self):
        """对象的信息"""
        return {
            "id": self.id,
            "name": self.name,
            "code": self.code,
            "desc": self.desc,
            "is_enabled": self.is_enabled,
            "is_deleted": self.is_deleted,
            "rights_ids": self.rights_ids,
            "create_datetime": self.create_datetime.strftime("%Y-%m-%d %H:%M:%S"),
            "update_datetime": self.update_datetime.strftime("%Y-%m-%d %H:%M:%S"),
        }
