from django.db import models

from ._base import BaseORM


class Department(BaseORM):
    name = models.CharField(verbose_name="部门名称", max_length=50)
    leader = models.CharField(verbose_name="负责人", max_length=50)
    pid = models.ForeignKey(verbose_name="上级部门ID", to='self', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "ums_department"

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "leader": self.leader,
            "pid": self.pid_id,
            "children": [],
            "create_datetime": self.create_datetime.strftime("%Y-%m-%d %H:%M:%S"),
            "update_datetime": self.update_datetime.strftime("%Y-%m-%d %H:%M:%S"),
            "is_deleted": self.is_deleted,
        }

    def json_department_tree(self):
        """这个用于用户展示界面,展示部门树,便于后续对用户的筛选!"""
        return {
            "id": self.id,
            "title": self.name,
            "pid": self.pid_id,
            "spread": True,  # 这样展示的权限树就是默认展开的!
            "children": [],
        }

    def json_tree(self):
        """下拉框展示的是树 layui第三方组件combotree拿到该数据后会自动构建树"""
        return {
            "id": self.id,
            "name": self.name,  # 注意
            "pid": self.pid_id,
        }
