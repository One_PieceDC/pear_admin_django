from django.db import models


class BaseORM(models.Model):
    create_datetime = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_datetime = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    is_deleted = models.BooleanField(verbose_name="是否删除 0-未删除 1-已删除", default=False)

    class Meta:
        abstract = True
