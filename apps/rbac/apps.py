from django.apps import AppConfig


class RbacConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.rbac'

    def ready(self):
        import apps.rbac.signals  # 导入包含信号接收器的模块
