from django.urls import path, include

# 对业务进行了拆分.
urlpatterns = [
    # - 静态页面,包括不仅限于首页、登录页、部门页、权限页等.
    path('', include('apps.rbac.views.static_html.routers')),
    # - Ajax请求的api接口
    path('', include('apps.rbac.views.account.routers')),  # 登陆、登出
    path('api/v1/', include('apps.rbac.views.department.routers')),  # 部门管理
    path('api/v1/', include('apps.rbac.views.rights.routers')),  # 权限管理
    path('api/v1/', include('apps.rbac.views.role.routers')),  # 角色管理
    path('api/v1/', include('apps.rbac.views.user.routers')),  # 用户管理
    path('api/v1/', include('apps.rbac.views.log.routers')),  # 日志管理
]
app_name = 'rbac'
