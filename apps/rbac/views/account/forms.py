from django import forms
from apps.base.models import User


class LoginForm(forms.ModelForm):
    """登陆"""
    mobile = forms.CharField(max_length=11, min_length=11)
    password = forms.CharField(min_length=6, max_length=8)

    class Meta:
        model = User
        fields = ['mobile', 'password']

    def validate_unique(self):
        return
