from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.login),  # 登陆
    path('logout/', views.logout),  # 登出
    path('menu/', views.get_menu),  # 获取菜单
]
