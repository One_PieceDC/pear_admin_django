from django.shortcuts import render
from django.views.decorators.http import require_GET

from apps.base import models
from django.conf import settings


# /login.html/ 登陆
@require_GET
def login_html(request):
    return render(request, 'login.html')


# / 首页
@require_GET
def index_html(request):
    return render(request, 'index.html')


# /view/analysis/index.html/  分析页
@require_GET
def analysis_html(request):
    return render(request, 'view/analysis/index.html')


# /view/console/index.html/ 工作台页
@require_GET
def console_html(request):
    return render(request, 'view/console/index.html')


"""部门模块"""


# /rbac/department/main.html/  部门主页
@require_GET
def department_main_html(request):
    return render(request, 'rbac/department/main.html')


# /rbac/department/add.html/  部门新增页
@require_GET
def department_add_html(request):
    return render(request, 'rbac/department/add.html')


# /rbac/department/edit.html/  部门修改页
@require_GET
def department_edit_html(request):
    edit_id = request.GET.get('id')
    edit_obj = models.Department.objects.filter(id=edit_id).first()
    return render(request, 'rbac/department/edit.html', {'edit_obj': edit_obj})


"""权限模块"""


# /rbac/rights/main.html/
@require_GET
def rights_main_html(request):
    return render(request, 'rbac/rights/main.html')


# rbac/rights/add.html/
@require_GET
def rights_add_html(request):
    return render(request, 'rbac/rights/add.html')


# rbac/rights/edit.html/
@require_GET
def rights_edit_html(request):
    edit_id = request.GET.get('id')
    edit_obj = models.Rights.objects.filter(id=edit_id).first()
    return render(request, 'rbac/rights/edit.html', {'edit_obj': edit_obj})


"""角色模块"""


# /rbac/role/main.html/
@require_GET
def role_main_html(request):
    return render(request, 'rbac/role/main.html')


# rbac/role/add.html/
@require_GET
def role_add_html(request):
    return render(request, 'rbac/role/add.html')


# rbac/role/edit.html/
@require_GET
def role_edit_html(request):
    edit_id = request.GET.get('id')
    edit_obj = models.Role.objects.filter(id=edit_id).first()
    return render(request, 'rbac/role/edit.html', {'edit_obj': edit_obj})


# rbac/role/power.html/
@require_GET
def role_power_html(request):
    role_id = request.GET.get('id')
    role_obj = models.Role.objects.filter(id=role_id).first()
    return render(request, 'rbac/role/power.html', {'role_obj': role_obj})


"""用户模块"""


# /rbac/user/main.html/
@require_GET
def user_main_html(request):
    return render(request, 'rbac/user/main.html')


# rbac/user/add.html/
@require_GET
def user_add_html(request):
    return render(request, 'rbac/user/add.html')


# rbac/user/edit.html/
@require_GET
def user_edit_html(request):
    edit_id = request.GET.get('id')
    edit_obj = models.User.objects.filter(id=edit_id).first()
    return render(request, 'rbac/user/edit.html', {
        'edit_obj': edit_obj,
        'default_path': settings.DEFAULT_AVATAR,
    })


# rbac/user/give_role.html/
@require_GET
def user_give_role_html(request):
    user_id = request.GET.get('id')
    user_obj = models.User.objects.filter(id=user_id).first()

    user_roles = user_obj.role_list.all()  # 用户的角色集合
    all_roles = models.Role.objects.filter(is_deleted=False, is_enabled=True)  # 获取数据库中所有的角色

    # 标记用户拥有的角色
    roles_with_flags = [
        {
            "id": role.id,
            "name": role.name,
            "is_owned": role in user_roles  # 如果当前用户拥有该角色,标记为 True
        }
        for role in all_roles
    ]
    return render(request, 'rbac/user/give_role.html', {'roles': roles_with_flags, 'user_obj': user_obj})


# rbac/user/reset_pwd.html/
@require_GET
def user_reset_pwd_html(request):
    user_id = request.GET.get('id')
    user_obj = models.User.objects.filter(id=user_id).first()
    return render(request, 'rbac/user/reset_pwd.html', {'user_obj': user_obj})


# rbac/user/info.html/
@require_GET
def user_info_html(request):
    user_id = request.auth
    user_obj = models.User.objects.filter(id=user_id).first()
    return render(request, 'rbac/user/info.html', {
        'user_obj': user_obj,
        'default_path': settings.DEFAULT_AVATAR,
    })


@require_GET
def log_main_html(request):
    """日志列表页面"""
    return render(request, 'rbac/log/main.html')
