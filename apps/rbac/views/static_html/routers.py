from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_html),  # 首页
    path('login.html/', views.login_html),  # 登陆页
    path('view/analysis/index.html/', views.analysis_html, name='analysis_index_html'),  # 分析页
    path('view/console/index.html/', views.console_html, name='console_index_html'),  # 工作台页

    path('rbac/department/main.html/', views.department_main_html, name='department_main_html'),  # 部门页
    path('rbac/department/add.html/', views.department_add_html, name='department_add_html'),  # 部门新增页
    path('rbac/department/edit.html/', views.department_edit_html, name='department_edit_html'),  # 部门修改页

    path('rbac/rights/main.html/', views.rights_main_html, name='rights_main_html'),  # 权限主页
    path('rbac/rights/add.html/', views.rights_add_html, name='rights_add_html'),  # 权限新增页
    path('rbac/rights/edit.html/', views.rights_edit_html, name='rights_edit_html'),  # 权限编辑页

    path('rbac/role/main.html/', views.role_main_html, name='role_main_html'),  # 角色页
    path('rbac/role/add.html/', views.role_add_html, name='role_add_html'),  # 角色新增页
    path('rbac/role/edit.html/', views.role_edit_html, name='role_edit_html'),  # 角色编辑页
    path('rbac/role/power.html/', views.role_power_html, name='role_power_html'),  # 角色授权页

    path('rbac/user/main.html/', views.user_main_html, name='user_main_html'),  # 用户页
    path('rbac/user/add.html/', views.user_add_html, name='user_add_html'),  # 用户新增页
    path('rbac/user/edit.html/', views.user_edit_html, name='user_edit_html'),  # 用户编辑页
    path('rbac/user/give_role.html/', views.user_give_role_html, name='user_give_role_html'),  # 用户授角页
    path('rbac/user/reset_pwd.html/', views.user_reset_pwd_html, name='user_reset_pwd_html'),  # 用户重置密码页
    # 用户个人中心
    path('rbac/user/info.html/', views.user_info_html, name='user_info_html'),

    # 日志模块
    path('rbac/log/main.html/', views.log_main_html, name='log_main_html'),  # 日志页
]
