from django.urls import path
from . import views

urlpatterns = [
    path('department/treetable/', views.get_list_as_treetable, name='department_list'),
    path('department/add/', views.department_add, name='department_add'),
    path('department/edit/', views.department_edit, name='department_edit'),
    path('department/del/', views.department_del, name='department_del'),
]
