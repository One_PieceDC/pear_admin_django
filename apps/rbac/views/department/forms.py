from django import forms
from apps.base.models import Department


class DepartmentAddForm(forms.ModelForm):
    """部门新增"""

    class Meta:
        model = Department
        fields = ['name', 'leader', 'pid']


class DepartmentEditForm(forms.ModelForm):
    """部门修改"""

    class Meta:
        model = Department
        fields = ['name', 'leader', 'pid']
