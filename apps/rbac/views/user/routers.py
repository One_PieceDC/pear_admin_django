from django.urls import path
from . import views

urlpatterns = [
    path('user/department_get/', views.user_department_get, name='user_department_get'),  # 获取部门树,以便后续的筛选 > layui的tree组件
    path('user/table/', views.get_list_as_table, name='user_list'),
    path('user/avatar/', views.user_avatar, name='user_avatar'),  # 上传用户头像
    path('user/get_dep_select/', views.get_dep_select, name='get_dep_select'),  # 获取部门下拉框列表 > 第三分组件combotree 我放到了pear里!!
    path('user/reset_pwd/', views.user_reset_pwd, name='user_reset_pwd'),  # 重置用户密码
    path('user/add/', views.user_add, name='user_add'),
    path('user/edit/', views.user_edit, name='user_edit'),
    path('user/del/', views.user_del, name='user_del'),
    path('user/is_enabled/', views.user_is_enabled, name='user_is_enabled'),
    path('user/role_update/', views.user_role_update, name='user_role_update'),  # 重新给用户分配角色
    # 用户个人中心
    path('user/info/', views.user_info, name='user_info'),  # 用户自己修改自己的基本信息
    path('user/iphone/', views.user_iphone, name='user_iphone'),  # 用户自己修改自己的手机号
    path('user/pwd/', views.user_pwd, name='user_pwd'),  # 用户自己修改自己的密码
]
