from django.urls import path
from . import views

urlpatterns = [
    path('rights/treetable/', views.get_list_as_treetable, name='rights_list'),
    path('rights/get_rights_pid_select/', views.get_rights_pid_select, name='get_rights_pid_select'),
    path('rights/add/', views.rights_add, name='rights_add'),
    path('rights/edit/', views.rights_edit, name='rights_edit'),
    path('rights/del/', views.rights_del, name='rights_del'),
    path('rights/is_enabled/', views.rights_is_enabled, name='rights_is_enabled'),
]