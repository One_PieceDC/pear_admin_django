from django import forms
from apps.base.models import Rights


class RightsAddEditForm(forms.ModelForm):
    """新增"""

    class Meta:
        model = Rights
        fields = ['title', 'code', 'kind', 'url', 'icon_sign', 'is_enabled', 'sort', 'open_type', 'remark', 'pid']

    def clean(self):
        # 所有字段自身校验以及所有字段钩子校验都通过的话,self.errors是为空的!
        if self.errors:
            return self.cleaned_data  # 源码中默认返回的就是它
        # 开始我们整体的校验逻辑!!
        """
        首先,kind字段自身就是不为空的.所以kind肯定有值.
        可以将项目左侧菜单理解成 
        -- 一级菜单`menu`
        -- 一级菜单`menu`
          -- 二级菜单`menu`
            -- 节点`menu-z`
            -- 三级菜单`menu`
               -- 节点`menu-z`
          -- 节点`menu-z`
             -- 权限`auth`
             -- 权限`auth`
          -- 节点`menu-z`
        > 规则怪谈: 新增的权限有三种类型-菜单menu、节点menu-z、权限auth。
          菜单可以放到任意层级的菜单下,也可以不选则自己作为顶级菜单 ；节点可以放到已有的任意层级的菜单下 ; 权限都必须放到节点下!!
        > 数据库中:
                  图标  权限标识  访问路径  pid是否必选
        "menu"     1      0       0        0
        "menu-z"   0      1       1        1 
        "auth"     0      1       1        1
        """
        kind = self.cleaned_data.get('kind')
        if kind == "menu":
            if not self.cleaned_data.get('icon_sign'):
                raise forms.ValidationError("权限类型为菜单,则必须设置图标!")
            pid_obj = self.cleaned_data.get('pid')
            if pid_obj and pid_obj.kind != "menu":
                raise forms.ValidationError("权限类型为菜单,选择的父级权限的类型必须为菜单!")
            self.cleaned_data['code'] = ""
            self.cleaned_data['url'] = ""
        elif kind == "menu-z":
            if not self.cleaned_data.get('code') or not self.cleaned_data.get('url'):
                raise forms.ValidationError("权限类型为节点,则必须设置权限标识和访问地址!")
            pid_obj = self.cleaned_data.get('pid')
            if not pid_obj:
                raise forms.ValidationError("权限类型为节点,则必须选择父级权限!")
            if pid_obj.kind != "menu":
                raise forms.ValidationError("权限类型为节点,选择的父级权限的类型必须为菜单!")
            self.cleaned_data['icon_sign'] = ""
        elif kind == "auth":
            if not self.cleaned_data.get('code') or not self.cleaned_data.get('url'):
                raise forms.ValidationError("权限类型为权限,则必须设置权限标识和访问地址!")
            pid_obj = self.cleaned_data.get('pid')
            if not pid_obj:
                raise forms.ValidationError("权限类型为权限,则必须选择父级权限!")
            if pid_obj.kind != "menu-z":
                raise forms.ValidationError("权限类型为权限,选择的父级权限的类型必须为节点!")
            self.cleaned_data['icon_sign'] = ""
        return self.cleaned_data
