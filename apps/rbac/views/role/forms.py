from django import forms
from apps.base.models import Role


class RoleAddForm(forms.ModelForm):
    """角色新增"""

    class Meta:
        model = Role
        fields = ['name', 'code', 'desc', 'is_enabled']


class RoleEditForm(forms.ModelForm):
    """角色编辑"""

    class Meta:
        model = Role
        fields = ['name', 'code', 'desc', 'is_enabled']
