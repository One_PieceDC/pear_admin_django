from django.urls import path
from . import views

urlpatterns = [
    path('role/table/', views.get_list_as_table, name='role_list'),
    path('role/add/', views.role_add, name='role_add'),
    path('role/edit/', views.role_edit, name='role_edit'),
    path('role/del/', views.role_del, name='role_del'),
    path('role/is_enabled/', views.role_is_enabled, name='role_is_enabled'),
    path('role/power_get/', views.role_power_get, name='role_power_get'),  # 获取权限树并根据角色的所拥有的权限进行选中
    path('role/power_update/', views.role_power_update, name='role_power_update'),  # 重新给角色分配权限
]
