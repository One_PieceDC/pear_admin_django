from django.urls import path
from . import views

urlpatterns = [
    path('log/table/', views.get_list_as_table, name='log_list'),  # 获取日志列表数据
    path('log/del/', views.log_del, name='log_del'),  # 删除日志
]
